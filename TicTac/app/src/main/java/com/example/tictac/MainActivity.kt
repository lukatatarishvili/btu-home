package com.example.tictac

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var isFirstPlayer = true


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun PlayAgain(){
        button00.text = ""
        button01.text = ""
        button02.text = ""
        button10.text = ""
        button11.text = ""
        button12.text = ""
        button20.text = ""
        button21.text = ""
        button22.text = ""

    }

    private fun init(){
        button00.setOnClickListener {
            move(button00)

        }
        button01.setOnClickListener {
            move(button01)

        }
        button02.setOnClickListener {
            move(button02)

        }
        button10.setOnClickListener {
            move(button10)

        }
        button11.setOnClickListener {
            move(button11)

        }
        button12.setOnClickListener {
            move(button12)

        }
        button20.setOnClickListener {
            move(button20)

        }
        button21.setOnClickListener {
            move(button21)

        }
        button22.setOnClickListener {
            move(button22)


        }

    }
    private fun move(button: Button){
        if(button.text.isEmpty()){
            if(isFirstPlayer){
                button.text= "X"
                isFirstPlayer=false
            }else{
                button.text= "O"
                isFirstPlayer=true
            }

        }
        if ( button00.text.isNotEmpty()&& button00.text==button01.text && button00.text==button02.text) {
            Toast.makeText(getApplicationContext(),"მოიგო ${button00.text}-მა",Toast.LENGTH_LONG).show();
            button00.isClickable = false
            PlayAgain()
        }
        else if (button10.text.isNotEmpty()&&button10.text==button11.text && button11.text==button12.text){
            Toast.makeText(getApplicationContext(),"მოიგო ${button10.text}-მა",Toast.LENGTH_LONG).show();
            button10.isClickable = false
            PlayAgain()
        }
        else if(button20.text.isNotEmpty()&& button20.text==button21.text && button20.text==button22.text){
            Toast.makeText(getApplicationContext(),"მოიგო ${button20.text}-მა",Toast.LENGTH_LONG).show();
            button20.isClickable = false
            PlayAgain()
        }
        else if (button00.text.isNotEmpty()&& button00.text==button10.text &&button00.text==button20.text){
            Toast.makeText(getApplicationContext(),"მოიგო ${button00.text}-მა",Toast.LENGTH_LONG).show();
            button00.isClickable = false
            PlayAgain()

        }
        else if(button10.text.isNotEmpty()&& button01.text==button11.text && button01.text==button21.text){
            Toast.makeText(getApplicationContext(),"მოიგო${button01.text}-მა",Toast.LENGTH_LONG).show();
            button10.isClickable = false
            PlayAgain()

        }
        else if (button02.text.isNotEmpty()&&button02.text==button12.text && button02.text==button22.text){
            Toast.makeText(getApplicationContext(),"მოიგო ${button02.text}-მა",Toast.LENGTH_LONG).show();
            button02.isClickable = false
            PlayAgain()

        }
        else if (button00.text.isNotEmpty()&& button00.text==button11.text && button11.text==button22.text) {
            Toast.makeText(getApplicationContext(),"მოიგო ${button00.text}-მა",Toast.LENGTH_LONG).show();
            button00.isClickable = false
            PlayAgain()
        }
        else if (button02.text.isNotEmpty()&& button02.text==button11.text && button02.text==button20.text){
            Toast.makeText(getApplicationContext(),"მოიგო ${button02.text}-მა",Toast.LENGTH_LONG).show();
            button02.isClickable = false
            PlayAgain()



        }


    }
    private fun disableClick(){
        button00.isClickable = false
        button01.isClickable = false
        button02.isClickable = false
        button10.isClickable = false
        button11.isClickable = false
        button12.isClickable = false
        button20.isClickable = false
        button21.isClickable = false
        button22.isClickable = false
    }
}
