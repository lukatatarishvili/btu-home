package com.example.calc

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }
    private fun init(){
        buttonOne.setOnClickListener(){
            Click(buttonOne)
        }
        buttonTwo.setOnClickListener(){
            Click(buttonTwo)
        }
        buttonThree.setOnClickListener(){
            Click(buttonThree)
        }
        buttonFour.setOnClickListener(){
            Click(buttonFour)

        }
        buttonFive.setOnClickListener(){
            Click(buttonFive)

        }
        buttonSix.setOnClickListener(){
            Click(buttonSix)

        }
        buttonSeven.setOnClickListener(){
            Click(buttonSeven)

        }
        buttonEight.setOnClickListener(){
            Click(buttonEight)

        }
        buttonNine.setOnClickListener(){
            Click(buttonNine)

        }
        buttonZero.setOnClickListener(){
            Click(buttonZero)

        }
    }

    private fun Click(button: Button) {
        resultBox.text = button.text
    }


}





